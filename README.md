# Utility-Belt
Personal packages on my PYTHONPATH that I import often in my projects.


# Demonstrates the following skills


## Software Testing Frameworks

### [jamesfulford/Utility-Belt/checks](https://github.com/jamesfulford/Utility-Belt/tree/master/checks)
Is a package to use with nosetests to check that functions give expected outputs for inputs.


## API Utilization

### [jamesfulford/Utility-Belt/datasource/currencies.py](https://github.com/jamesfulford/Utility-Belt/tree/master)currencies.py]
Uses a free currency exchange rate API. Builds a class for each currency (dynamically) and adds them to the local variable scope.

### [jamesfulford/Utility-Belt/visualization/googlemaps.py](https://github.com/jamesfulford/Utility-Belt/tree/master)visualization/googlemaps.py]
Uses the staticmap endpoint of maps.googleapis.com to make map images with pins.

### [jamesfulford/Utility-Belt/engage](https://github.com/jamesfulford/Utility-Belt/tree/master/engage)
Uses a wrapper library for the bufferapp.com API to assist in the automation of social media marketing.


## Data Manipulation

### [jamesfulford/Utility-Belt/analytics/dataset](https://github.com/jamesfulford/Utility-Belt/tree/master/analytics)dataset]
Is a package for accessing particular values of datapoints and managing datasets.

### [jamesfulford/Utility-Belt/engage](https://github.com/jamesfulford/Utility-Belt/tree/master/engage)
Uses json for data persistence.


## Report Generation

### [jamesfulford/Utility-Belt/emailer](https://github.com/jamesfulford/Utility-Belt/tree/master/emailer)
Uses gmail servers to send emails

### [jamesfulford/Utility-Belt/paper](https://github.com/jamesfulford/Utility-Belt/tree/master/paper)
Uses docx package to write papers from template Word document.


## Scientific Programming

### [jamesfulford/Utility-Belt/modelling](https://github.com/jamesfulford/Utility-Belt/tree/master/modelling)
Uses numpy to fit various functions (lines, polynomials, logistic curves, etc.) to given datasets.
